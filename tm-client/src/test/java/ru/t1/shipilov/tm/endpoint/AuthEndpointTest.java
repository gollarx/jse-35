package ru.t1.shipilov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.dto.request.UserLoginRequest;
import ru.t1.shipilov.tm.dto.request.UserLogoutRequest;
import ru.t1.shipilov.tm.dto.request.UserProfileRequest;
import ru.t1.shipilov.tm.dto.response.UserLoginResponse;
import ru.t1.shipilov.tm.dto.response.UserLogoutResponse;
import ru.t1.shipilov.tm.dto.response.UserProfileResponse;
import ru.t1.shipilov.tm.marker.SoapCategory;
import ru.t1.shipilov.tm.service.PropertyService;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Test
    public void testLogin() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(
                new UserLoginRequest(null, null))
        );
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
    }

    @Test
    public void testLogout() {
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        @NotNull final UserLogoutResponse userLogoutResponse = authEndpoint.logout(
                new UserLogoutRequest(userLoginResponse.getToken())
        );
        Assert.assertNotNull(userLogoutResponse);
    }

    @Test
    public void testProfile() {
        @Nullable final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        @Nullable final UserProfileResponse userProfileResponse = authEndpoint.profile(
                new UserProfileRequest(userLoginResponse.getToken())
        );
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
    }

}
