package ru.t1.shipilov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.request.DataBinaryLoadRequest;

public final class DataBinaryLoadCommand extends AbstractDataCommand{

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    public static final String DESCRIPTION = "Load data from binary file.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(getToken());
        getDomainEndpoint().loadDataBinary(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public  String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
