package ru.t1.shipilov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.request.DataBackupSaveRequest;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    public static final String NAME = "backup-save";

    public static final String DESCRIPTION = "Save backup to file.";

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(getToken());
        getDomainEndpoint().saveDataBackup(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
