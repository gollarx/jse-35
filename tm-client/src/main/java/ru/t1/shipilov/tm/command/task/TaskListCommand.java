package ru.t1.shipilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.request.TaskListRequest;
import ru.t1.shipilov.tm.enumerated.Sort;
import ru.t1.shipilov.tm.exception.entity.TaskNotFoundException;
import ru.t1.shipilov.tm.model.Task;
import ru.t1.shipilov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-list";

    @NotNull
    private final String DESCRIPTION = "Show task list.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken(), sort);
        @Nullable final List<Task> tasks = getTaskEndpoint().listTask(request).getTasks();
        if (tasks != null) throw new TaskNotFoundException();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
