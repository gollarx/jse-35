package ru.t1.shipilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.dto.request.UserLogoutRequest;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "logout";

    @NotNull
    private final String DESCRIPTION = "Logout current user.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        getAuthEndpoint().logout(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
