package ru.t1.shipilov.tm.exception.entity.field;

public class StatusEmptyException extends AbstractFieldException {

    public StatusEmptyException() {
        super("Error! Status is empty...");
    }

}
