package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.model.Session;
import ru.t1.shipilov.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    Session validateToken(@Nullable String token);

    void invalidate(@Nullable Session session);

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
