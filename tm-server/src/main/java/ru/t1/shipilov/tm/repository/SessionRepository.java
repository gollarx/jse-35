package ru.t1.shipilov.tm.repository;

import ru.t1.shipilov.tm.api.repository.ISessionRepository;
import ru.t1.shipilov.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}
